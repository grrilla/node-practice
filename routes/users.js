var pg = require('pg');
var express = require('express');
var router = express.Router();

var conString = 'postgres://postgres:postgres@localhost:5432/practice';
var client = new pg.Client(conString);
client.connect();
console.log('Client connected');

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

router.post('/', function(req, res, next) {
  var uname = req.body.name;
  var passwd = req.body.password;

  client.query('INSERT INTO users VALUES($1::text, $2::text)', [uname, passwd], function(err, result) {
    if (err) console.error(err);

    console.log('Result: ', result);
  });

});

module.exports = router;
